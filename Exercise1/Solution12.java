
package Level12;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Solution12 {
    public static void main(String[] args) throws IOException {

        // указываем нужную директорию для работы
        String directoryName = "C:\\Users\\user\\Downloads";
        // указываем имя файла, в который планируем поместить результат и где он располагается
        String resultFileName = "C:\\Users\\user\\Downloads\\mergedfile.txt";

        // прописываем filter для поиска по директориям и именам всех файлов с расширением txt
        FilenameFilter txtFilter = (File dir, String name) -> {
            return (new File(dir.getAbsolutePath() + File.separator + name).isDirectory()) || name.toLowerCase().endsWith(".txt");
        };

        List<File> textFileNameList = getFileName(new File(directoryName), new ArrayList<>(), txtFilter);
        Collections.sort(textFileNameList, new SortFileName());
        String resultText = fileWriter(textFileNameList, Charset.forName("UTF-8"));
        fileWriter(resultFileName, resultText);
    }

    // описываем метод для работы с файлами (получаем и добавляем)
    public static List<File> getFileName(File directory, List<File> fileNameList, FilenameFilter txtFilter) {
        for (File file : directory.listFiles(txtFilter)) {
            if (file.isDirectory()) getFileName(file, fileNameList, txtFilter);
            else fileNameList.add(file);
        }
        return fileNameList;
    }

    //  описываем метод записи в файл
    public static String fileWriter(List<File> textFileNameList, Charset charset) {
        final StringBuilder allText = new StringBuilder();
        textFileNameList.forEach((file) -> {
            //блок try-catch для обработки ввода
            try {
                Files.lines(Paths.get(file.getAbsolutePath()), charset).
                        forEach((t) -> {allText.append(t).append("\r\n");});
            } catch (IOException ex) {
                Logger.getLogger(Solution12.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        return allText.toString();
    }

    public static boolean fileWriter(String fileName, String text) throws IOException {
        //блок try-catch для обработки вывода
        try (BufferedOutputStream bof = new BufferedOutputStream(new FileOutputStream(fileName))) {
            bof.write(text.getBytes());
        } catch (IOException e) {
            return false;
        }
        return true;
    }
}

// подключаем сериализацию
class SortFileName implements Serializable, Comparator<File>{
    // явно прописываем serialVersionUID
    // чтобы в неизменяющемся классе serialVersionUID системой определялся каждый раз одинаковым
    private static final long serialVersionUID = -1L;

    @Override
    public int compare(File file1, File file2) {
        return file1.getName().compareTo(file2.getName());
    }

}
