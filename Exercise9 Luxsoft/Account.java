
public class Account {
    private final int id;
    private double balance = 0.0;

    public Account (int id) {
        this.id = id;
    }
    public Account(int id, double balance) {
        this.id = id;
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    //снять количество денег
    public void withdraw(double amount) {
        if(balance >= amount) {
            balance -= amount;
        }
    }

    //положить количество денег
    public void deposit(double amount) {
        balance += amount;
    }
    public int getId(){
        return id;
    }
}