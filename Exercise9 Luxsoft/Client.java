import java.util.Arrays;

public class Client {
    private final String name;
    private Account[] accounts = new Account[0];

    public Client(String name) { this.name = name;
    }

    public Account[] getAccounts() {
        return accounts;
    }

    public void addAccount(Account account){
        accounts = Arrays.copyOf(accounts, accounts.length + 1);
        accounts[accounts.length - 1] = account;
    }
    public String getName(){
        return name;
    }
}
