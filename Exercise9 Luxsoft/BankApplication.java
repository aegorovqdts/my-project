
public class BankApplication {
    public static void main(String[] args) {
        Bank bank = new Bank();
        Client client1 = new Client("Client 1");
        client1.addAccount(new Account(1, 0.0));
        Account account = new Account(2, 500.0);
        account.withdraw(100);
        client1.addAccount(account);

        bank.addClient(client1);
        Client client2 = new Client("Client 2");
        client2.addAccount(new Account(12, 100.0));
        bank.addClient(client2);
        Client client3 = new Client("Client 3");
        client3.addAccount(new Account(123, 10000.0));
        bank.addClient(client3);

        printBalance(bank);

        modifyBank(bank);

        printBalance(bank);
    }

    static void printBalance(Bank bank) {
        for (Client client : bank.getClients()) {
            System.out.println("==========================");
            System.out.println("Client " + client.getName());
            System.out.println("--------------------------");
            for (Account account : client.getAccounts()) {
                System.out.println("Account " + account.getId() + " Balance:" + account.getBalance() + "$");
            }
            System.out.println("==========================");
            }
    }

    static void modifyBank(Bank bank){
        for (Client client : bank.getClients()) {
            Account[] accounts = client.getAccounts();
            Account account = accounts[0];
            account.deposit(100);
        }
        System.out.println("---=== Bank modified ===---");
    }
}