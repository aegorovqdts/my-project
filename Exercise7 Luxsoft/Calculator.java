
public class Calculator {
    public static char[] ArrayFirst = {'0','a','1','b','2','c','3','d','4','e'};

    //проверяем элементы и их количество
    public static boolean checkNumber(String arg) {
        int AllIn = 0;
        for (int i = 0; i < arg.length(); i++) {
            char c = arg.charAt(i);
            for (char p : ArrayFirst) {
                if (c == p) {
                    AllIn++;
                }
            }
        }
        if (AllIn == arg.length()) {
            return true;
        }
        return false;
    }

    // main метод
    public static void main(String[] args) {
        double result;
        double a = 0;
        double b = 0;

        if (args.length != 3) {
            System.out.println("Программа получила три аргумента");
            return;
        }

        if(!checkNumber(args[0]) || !checkNumber(args[2])) {
            System.out.println("первый и третий элементы должны быть числами, а второй любым символом");
            return;
        }

        a = Double.parseDouble(args[0]);
        b = Double.parseDouble(args[2]);

        switch (args[1]) {
            case "сумма + " :
                result = a + b;
                break;
            case "разница - " :
                result = a - b;
                break;
            case "умножение * " :
                result = a * b;
                break;
            case "деление / " :
                result = a / b;
                break;
            default:
                System.out.println("Неподходящее значение");
                return;
        }
        System.out.format("Результат: ", result);
    }
}
